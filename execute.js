import { check, sleep } from "k6";
import http from "k6/http";

let testFile = __ENV.TEST_FILE || "./tests/staging.json";

const tests = JSON.parse(open(testFile));

export default function() {
  let url = tests[Math.floor(Math.random() * tests.length)];

  var r = http.get(url, { headers: { "Private-Token": __ENV.GITLAB_TOKEN } });

  check(r, {
    "status is 200": r => r.status === 200
  });
}
