# Load Testing Toolkit for GitLab

A load testing toolkit for GitLab using [k6](https://k6.io/).

## How to use

1. Install `docker-compose`
1. Export a private token: `export GITLAB_TOKEN=pa55word`
1. Run tests against Staging: `docker-compose run k6 run -d 1m -u 10 /src/execute.js`
   * In this example, the test will emulate 10 users (`-u 10`) for 1 minute (`-d 1m`)
   * `docker-compose run k6 run --help` for more details

## Generating new test scripts

The toolkit comes with a simple crawler for generating new test scripts. It can be run as follows:

```shell
docker-compose run creeper -count 10 -instance https://gitlab.example.com -match '/raw/' > ./tests/example-raw.json
```

Remember to export your `GITLAB_TOKEN` before starting the creeper.

The arguments are all optional. They are:

* `-instance`: The GitLab instance to crawl.
* `-match`: A regular expression to match in the URL. Only URLs that match this will be collected.
* `-count`: The number of URLs to collect before stopping.

This will generate a test script. The test script can be executed as follows:

```shell
docker-compose run k6 run -d 1m -u 10 -e TEST_FILE=./tests/example-raw.json /src/execute.js
```
