package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"regexp"
	"strings"
	"sync"

	"github.com/gocolly/colly"
)

var logger = log.New(os.Stderr, "", 0)

var count = flag.Int("count", 10, "number of urls to collect")
var match = flag.String("match", "", "only collect URLs which contain the given string")
var instance = flag.String("instance", "https://staging.gitlab.com", "Starting URL of the GitLab instance to crawl")

var urls = []string{}
var m sync.Mutex
var wg sync.WaitGroup

// addURL returns true if we should continue
func addURL(u string) bool {
	m.Lock()
	defer m.Unlock()

	if len(urls) >= *count {
		return false
	}

	if *match == "" {
		urls = append(urls, u)
	} else {
		matched, err := regexp.MatchString(*match, u)
		if err != nil {
			panic(err)
		}

		if matched {
			urls = append(urls, u)
		}
	}

	if len(urls) == *count {
		wg.Done()
	}

	return true
}

func main() {
	flag.Parse()

	instanceURL, err := url.Parse(*instance)
	if err != nil {
		panic(err)
	}

	c := colly.NewCollector(
		colly.IgnoreRobotsTxt(),
		colly.AllowedDomains(instanceURL.Host),
		colly.Async(true),
	)

	c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: 10})

	wg.Add(1)

	// visit all links
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		fullLink := e.Request.AbsoluteURL(link)
		u, err := url.Parse(fullLink)
		if err != nil {
			return
		}

		// Ignore atom feeds
		if strings.HasSuffix(u.Path, ".atom") {
			return
		}

		// Ignore help docs
		if strings.HasPrefix(u.Path, "/help/") {
			return
		}

		// Ignore users
		if strings.HasPrefix(u.Path, "/users/") {
			return
		}

		if u.RawQuery != "" {
			if m, err := url.ParseQuery(u.RawQuery); err == nil {
				if m.Get("sort") != "" {
					return
				}
			}
		}

		c.Visit(e.Request.AbsoluteURL(link))
	})

	c.OnRequest(func(r *colly.Request) {
		if os.Getenv("GITLAB_TOKEN") != "" {
			r.Headers.Set("Private-Token", os.Getenv("GITLAB_TOKEN"))
		}

		if !addURL(r.URL.String()) {
			r.Abort()
		}
	})

	logger.Printf("Starting crawl at %v", *instance)
	if *match != "" {
		logger.Printf("Matching URLs like %v", *match)
	}
	logger.Printf("Will stop after %v urls", *count)

	c.Visit(*instance)
	wg.Wait()

	bytes, err := json.Marshal(urls)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(bytes))
}
